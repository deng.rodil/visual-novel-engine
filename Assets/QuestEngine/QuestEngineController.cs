using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace QuestEngine {
	public class QuestEngineController : MonoBehaviour {
		
		public List<GameObject> Listeners;

		private List<Quest> trackedQuests = new List<Quest>();
		public QuestDatabase Database { get; private set; }

		void Awake()
		{
			Database = GetComponent<QuestDatabase>();
			//Database.LoadAll();
			
			// Load user's progress here
		}
		
		// Use this for initialization
		void Start () {
			foreach (Quest quest in trackedQuests){
				quest.Activated += OnQuestActivated;
				quest.TasksCompleted += OnQuestTasksCompleted;
				quest.Failed += OnQuestFailed;
				quest.Updated += OnQuestUpdated;
			}
		}
		
		// Update is called once per frame
		void Update () {
			foreach (Quest quest in trackedQuests){
				quest.Tick(Time.deltaTime);
			}
		}

		public void AttachQuest(Quest quest)
		{
			//if (quest.Status == QuestStatus.Completed || QuestStatus.Failed) throw new UnityException("Cannot attach a completed of failed quest");

			quest.Activated += OnQuestActivated;
			quest.TasksCompleted += OnQuestTasksCompleted;
			quest.Failed += OnQuestFailed;
			quest.Updated += OnQuestUpdated;
			quest.Rewarding += OnQuestRewarding;
			trackedQuests.Add(quest);
		}

		public void DetachQuest(Quest quest)
		{
			quest.Activated -= OnQuestActivated;
			quest.TasksCompleted -= OnQuestTasksCompleted;
			quest.Failed -= OnQuestFailed;
			quest.Updated -= OnQuestUpdated;
			trackedQuests.Remove(quest);
		}
		
		void NotifyListeners(string message, object obj)
		{
			foreach (GameObject listener in Listeners)
			{
				listener.SendMessage(message, obj, SendMessageOptions.DontRequireReceiver);
			}
		}
		
		#region EventHandlers
		void OnQuestActivated(Quest quest, IEnumerable<QuestCommand> commands)
		{
			NotifyListeners("QuestActivated", quest);
			HandleCommands(commands);
		}
		
		void OnQuestTasksCompleted(Quest quest, IEnumerable<QuestCommand> commands)
		{
			NotifyListeners("QuestTasksCompleted", quest);
		}
		
		void OnQuestFailed(Quest quest, IEnumerable<QuestCommand> commands)
		{
			NotifyListeners("QuestFailed", quest);
			HandleCommands(commands);
		}
		
		void OnQuestUpdated(Quest quest)
		{
			NotifyListeners("QuestUpdated", quest);
		}

		void OnQuestRewarding(Quest quest, IEnumerable<QuestCommand> commands)
		{
			NotifyListeners("QuestRewarding", quest);
			HandleCommands(commands);
		}
		
		void HandleCommands(IEnumerable<QuestCommand> commands)
		{
			foreach(QuestCommand command in commands){
				// Activate the quest (waiting for Quest Database)
				if (command.Name == "ActivateQuest"){
					
				}
				
				NotifyListeners("ExecuteQuestCommand", command);
			}
		}
		
		public void HandleEvent(QuestEvent qEvent)
		{
			foreach (Quest quest in trackedQuests.ToArray()){
				quest.HandleEvent(this, qEvent);
			}
		}
		#endregion
	}
}
