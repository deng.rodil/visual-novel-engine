﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace QuestEngine {
	public class QuestEvent {
		/// <summary>
		/// List of all parameters related to this event.
		/// </summary>
		private Dictionary<string, string> parameters = new Dictionary<string, string>();
		[JsonProperty("params")]
		public IDictionary<string, string> Params { get { return parameters; } }
		/// <summary>
		/// Name of this event.
		/// </summary>
		/// <value>The name.</value>
		[JsonProperty("name")]
		public string Name { get; set; }
		/// <summary>
		/// Used to quantize this event. (eg. Collect 10 Jellopies)
		/// </summary>
		/// <value>The count.</value>
		[JsonProperty("count")]
		public int Count { get; set; }
		
		[JsonIgnoreAttribute]
		public bool Quantized
		{
			get { return Count > 0; }
		}
		
		public bool Equivalent(QuestEvent other)
		{
			if (Name != other.Name) return false;
			
			foreach (KeyValuePair<string,string> kv in Params){
				if (!other.Params.ContainsKey(kv.Key)){
					return false;
				}
				if (other.Params[kv.Key] != kv.Value){
					return false;
				}
			}
			
			return true;
		}
	}
}
