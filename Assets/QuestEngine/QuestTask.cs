﻿using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

namespace QuestEngine {
	public enum QuestTaskStatus {
		Active,
		Completed,
		Failed
	}
	
	public class QuestTask {
		#region BackingFields
		private List<QuestEvent> successEvents = new List<QuestEvent>();
		private List<QuestEvent> failEvents = new List<QuestEvent>();
		#endregion
		
		/// <summary>
		/// List of all the events that can bump this task. The client only needs to fulfill one of these events.
		/// </summary>
		[JsonProperty("events_success")]
		public IList<QuestEvent> SuccessEvents { get { return successEvents; } }
		/// <summary>
		/// List of all the events that can fail this task. The client only needs to fulfill one of these events.
		/// </summary>
		[JsonProperty("events_fail")]
		public IList<QuestEvent> FailEvents { get { return failEvents; } }

		/// <summary>
		/// Unique identifier for this task.
		/// </summary>
		/// <value>The identifier.</value>
		[JsonProperty("id")]
		public string Id { get; set; }
		/// <summary>
		/// Display name for this task.
		/// </summary>
		/// <value>The name.</value>
		[JsonProperty("name")]
		public string Name { get; set; }
		/// <summary>
		/// Status of this task.
		/// </summary>
		/// <value>The status.</value>
		[JsonProperty("status")]
		public QuestTaskStatus Status { get; private set; }
		/// <summary>
		/// Required number of repetition to complete this task. (eg. Kill 5 Porings)
		/// </summary>
		/// <value>The count.</value>
		[JsonProperty("count")]
		public int Count { get; set; }
		/// <summary>
		/// Current count progress of this task. (eg. Kill 5 Porings (3/5))
		/// </summary>
		/// <value>The progress.</value>
		[JsonProperty("progress")]
		public int Progress { get; private set; }
		
		/// <summary>
		/// Called by the Quest that holds this task. This is used to handle all messages sent by the client.
		/// </summary>
		/// <returns><c>true</c>, if this task's state is changed, <c>false</c> otherwise.</returns>
		/// <param name="qEvent">Q event.</param>
		public bool HandleEvent(QuestEvent qEvent)
		{
			if (Status != QuestTaskStatus.Active) return false;
			
			// Check if a fail event was passed
			foreach (QuestEvent q in FailEvents){
				if (q.Equivalent(qEvent)){
					Fail();
					return true;
				}
			}
			
			// Check if passed event is similar to one of the events that this task requires
			bool triggered = false;
			foreach (QuestEvent q in SuccessEvents){
				// Check if events matched
				if (q.Equivalent(qEvent)){
					triggered = true;
					break;
				}
			}
			
			if (triggered){
				// Events that are quantized will increment progress by count data. Else, increment current progress by 1.
				if (qEvent.Quantized){
					Progress += qEvent.Count;
				} else {
					Progress++;
				}
				
				if (Progress >= Count){
					Complete();
				}
				
				return true;
			}
			
			return false;
		}
		
		/// <summary>
		/// Complete this task. This should only be called by Quest.
		/// </summary>
		public void Complete()
		{
			Progress = Count;
			Status = QuestTaskStatus.Completed;
		}
		
		/// <summary>
		/// Fail this task. This should only be called by Quest.
		/// </summary>
		public void Fail()
		{
			Status = QuestTaskStatus.Failed;
		}

		public JObject SerializeUserData()
		{
			JObject json = new JObject();
			json["id"] = Id;
			json["status"] = Status.ToString();
			json["progress"] = Progress.ToString();

			return json;
		}

		public void DeserializeUserData(JObject json)
		{
			if ((string)json["id"] != Id) throw new System.Exception("Cannot deserialize QuestTask from a different id");
			Status = (QuestTaskStatus)Enum.Parse(typeof(QuestTaskStatus), (string)json["status"]);
			Progress = int.Parse((string)json["progress"]);
		}
	}
}
