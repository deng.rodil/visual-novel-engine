﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace QuestEngine {
	public class QuestDatabase : MonoBehaviour {
		public List<TextAsset> Data;

		private Dictionary<string, Quest> quests = new Dictionary<string, Quest>();
		private DirectoryInfo dbDir;

		public IEnumerable<Quest> AllQuests { get { return quests.Values; } }

		void Awake()
		{
			dbDir = new DirectoryInfo(Application.dataPath + "/gamedata/quests");
			if (!Directory.Exists(dbDir.FullName)) Directory.CreateDirectory(dbDir.FullName);
		}
		
		public IEnumerable<Quest> LoadAll()
		{
			// Load bundled quests
			foreach (TextAsset asset in Data){
				try {
					DeserializeQuest(asset.text);
				}
				catch (System.Exception e){
					Debug.Log(e.StackTrace);
				}
			}

			// Load external quests
			foreach (FileInfo file in dbDir.GetFiles()){
				if (file.Extension == ".json")
				{
					try
					{
						// Read the text within the file
						using (StreamReader reader = file.OpenText())
						{
							string jsonString = reader.ReadToEnd();
							DeserializeQuest(jsonString);
						}
					}
					catch (System.Exception e)
					{
						Debug.Log(e.Message + "\n" + e.StackTrace);
					}
				}
			}
			
			Debug.Log("Quests Loaded: " + quests.Count);
			return quests.Values;
		}
		
		public void Add(Quest quest)
		{		
			// Open file stream
			FileInfo file = new FileInfo(dbDir.FullName + "/" + quest.Id + ".json");
			if (file.Exists) file.Delete();
			
			// Serialize
			try
			{
				using (FileStream stream = file.Create())
				{
					string json = JsonConvert.SerializeObject(quest);
					
					byte[] data = Encoding.UTF8.GetBytes(json);
					stream.Write(data, 0, data.Length);
				}

				if (quests.ContainsKey(quest.Id)){
					quests.Remove(quest.Id);
				}
				quests [quest.Id] = quest;
				Debug.Log("Quest added: " + quest);
			}
			catch (System.Exception e)
			{
				Debug.Log(e.StackTrace);
			}
		}

		void DeserializeQuest(string json)
		{
			JToken obj = JToken.Parse(json);

			if (obj.Type == JTokenType.Array){
				foreach (JToken token in obj.Children()){
					Quest quest = JsonConvert.DeserializeObject<Quest>(token.ToString());
					if (quests.ContainsKey(quest.Id)){
						quests.Remove(quest.Id);
					}
					quests.Add(quest.Id, quest);
				}
				
			} else if (obj.Type == JTokenType.Object) {
				Quest quest = JsonConvert.DeserializeObject<Quest>(json);
				if (quests.ContainsKey(quest.Id)){
					quests.Remove(quest.Id);
				}
				quests.Add(quest.Id, quest);
			}
		}
	}

}