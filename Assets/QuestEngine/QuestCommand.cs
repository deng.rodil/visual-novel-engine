using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace QuestEngine {
	public class QuestCommand {
		/// <summary>
		/// Parameters for this Command to be processed by the client.
		/// </summary>
		private Dictionary<string, string> parameters = new Dictionary<string, string>();
		[JsonProperty("params")]
		public IDictionary<string, string> Params { get { return parameters; } }
		/// <summary>
		/// Name of this command.
		/// </summary>
		/// <value>The name.</value>
		[JsonProperty("name")]
		public string Name { get; set; }
	}
}
