using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace QuestEngine {
	public enum QuestStatus {
		Locked,
		Active,
		Completed,
		Failed,
		Rewarded
	}
	
	public class Quest {
		#region BackingFields
		private List<QuestTask> tasks = new List<QuestTask> ();
		private List<QuestCommand> activationCommands = new List<QuestCommand> ();
		private List<QuestCommand> completionCommmands = new List<QuestCommand> ();
		private List<QuestCommand> failureCommands = new List<QuestCommand> ();
		private List<QuestCommand> rewardCommands = new List<QuestCommand>();
		#endregion
		
		/// <summary>
		/// List of all tasks to complete. Failing one task will mean quest failure.
		/// </summary>
		[JsonProperty(PropertyName="tasks")]
		public IList<QuestTask> Tasks { get { return tasks; } }
		/// <summary>
		/// Commands sent to the client upon activating the quest.
		/// </summary>
		[JsonProperty(PropertyName="commands_activation")]
		public IList<QuestCommand> ActivationCommands { get { return activationCommands; } }
		/// <summary>
		/// Commands sent to the client upon completing all quest tasks.
		/// </summary>
		[JsonProperty(PropertyName="commands_completion")]
		public IList<QuestCommand> CompletionCommands { get { return completionCommmands; } }
		/// <summary>
		/// Commands sent to the client upon failing the quest.
		/// </summary>
		[JsonProperty(PropertyName="commands_failure")]
		public IList<QuestCommand> FailureCommands { get { return failureCommands; } }
		/// <summary>
		/// Commands sent to the client upon failing the quest.
		/// </summary>
		[JsonProperty(PropertyName="commands_reward")]
		public IList<QuestCommand> RewardCommands { get { return rewardCommands; } }
		
		/// <summary>
		/// Unique identifier for this quest.
		/// </summary>
		/// <value>The identifier.</value>
		[JsonProperty(PropertyName="id")]
		public string Id { get; set; }
		/// <summary>
		/// Additional identifier for quest. Can be used to mark quest as a part of a route.
		/// </summary>
		/// <value>The category.</value>
		[JsonProperty(PropertyName="category")]
		public string Category { get; set; }
		/// <summary>
		/// Status of the quest.
		/// </summary>
		/// <value>The status.</value>
		[JsonProperty(PropertyName="status")]
		public QuestStatus Status { get; private set; }
		/// <summary>
		/// Display name for the quest.
		/// </summary>
		/// <value>The name.</value>
		[JsonProperty(PropertyName="name")]
		public string Name { get; set; }
		/// <summary>
		/// Description of the quest.
		/// </summary>
		/// <value>The description.</value>
		[JsonProperty(PropertyName="description")]
		public string Description { get; set; }
		/// <summary>
		/// Determines if this quest is timed. Duration can be ignored if this is false.
		/// </summary>
		/// <value><c>true</c> if timed; otherwise, <c>false</c>.</value>
		[JsonProperty(PropertyName="timed")]
		public bool Timed { get; set; }
		/// <summary>
		/// Timer of the quest upon activation. Ignore this if Timed is set to false.
		/// </summary>
		/// <value>The duration.</value>
		[JsonProperty(PropertyName="duration")]
		public float Duration { get; set; }
		/// <summary>
		/// Time elapsed since the activation of the quest.
		/// </summary>
		/// <value>The time elapsed.</value>
		[JsonProperty(PropertyName="time_elapsed")]
		public float TimeElapsed { get; set; }
		[JsonProperty(PropertyName="turn_in_task")]
		public QuestTask TurnInTask { get; set; }

		#region Events
		[JsonIgnoreAttribute]
		public System.Action<Quest, IEnumerable<QuestCommand>> Activated;
		[JsonIgnoreAttribute]
		public System.Action<Quest, IEnumerable<QuestCommand>> TasksCompleted;
		[JsonIgnoreAttribute]
		public System.Action<Quest, IEnumerable<QuestCommand>> Failed;
		[JsonIgnoreAttribute]
		public System.Action<Quest, IEnumerable<QuestCommand>> Rewarding;
		[JsonIgnoreAttribute]
		public System.Action<Quest, IEnumerable<QuestCommand>> TurningOver;
		[JsonIgnoreAttribute]
		public System.Action<Quest> Locked;
		[JsonIgnoreAttribute]
		public System.Action<Quest> Updated;
		#endregion
		
		/// <summary>
		/// Activates the quest. Sends an event to the engine.
		/// </summary>
		public void Activate()
		{
			// Will only work if the quest is locked
			if (Status != QuestStatus.Locked) return;
			
			Status = QuestStatus.Active;
			if (Activated != null) Activated(this, ActivationCommands);
		}
		
		/// <summary>
		/// Locks the quest. Sends an event to the engine.
		/// </summary>
		public void Lock()
		{
			// Will only work if the quest is active
			if (Status != QuestStatus.Active) return;
			
			Status = QuestStatus.Locked;
			if (Locked != null) Locked(this);
		}
		
		/// <summary>
		/// Completes the quest. Sends an event to the engine.
		/// </summary>
		public void Complete()
		{
			// Will only work if the quest is active
			if (Status != QuestStatus.Active) return;
			
			// Complete all the tasks
			foreach (QuestTask task in Tasks){
				task.Complete();
			}
			
			// Complete the quest
			this.Status = QuestStatus.Completed;
			if (TasksCompleted != null) TasksCompleted(this, CompletionCommands);
		}
		
		/// <summary>
		/// Fails the quest. Sends an event to the engine.
		/// </summary>
		public void Fail()
		{
			this.Status = QuestStatus.Failed;
			if (Failed != null) Failed(this, FailureCommands);
		}

		public void TurnIn()
		{
			// Will only work if the quest is completed
			if (Status != QuestStatus.Completed) return;

			// Handler for TurnIn
			if (TurnInTask != null)
			{
				TurnInTask.Complete();
			}

			this.Status = QuestStatus.Rewarded;
			if (Rewarding != null) Rewarding(this, RewardCommands);
		}
		
		public void HandleEvent(QuestEngineController engine, QuestEvent qEvent)
		{
			// Handler for turn-in events
			if (Status == QuestStatus.Completed && TurnInTask != null)
			{
				TurnInTask.HandleEvent(qEvent);
				if (TurnInTask.Status == QuestTaskStatus.Completed)
				{
					TurnIn();
				}

				return;
			}

			// Events are only handled for active quests
			if (Status != QuestStatus.Active) return;
			
			// Send the message to all quest tasks and check if any was updated
			bool updated = false;
			foreach (QuestTask task in Tasks)
			{
				updated |= task.HandleEvent(qEvent);
			}
			
			// If a task was updated, do the necessary checks
			if (updated){
				if (Updated != null) Updated(this);
				
				
				// Check status of all tasks
				bool tasksCompleted = true;
				bool failed = false;
				foreach (QuestTask task in Tasks){
					switch (task.Status){
					case QuestTaskStatus.Active:
						tasksCompleted = false;
						break;
					case QuestTaskStatus.Failed:
						failed = true;
						break;
					default:
						break;
					}
				}
				
				// Check if task failed
				if (failed){
					Fail();
				}
				// Complete quest if all tasks are cleared
				else if (tasksCompleted){
					Complete();
				}
			}
		}
		
		public void Tick(float dt)
		{
			if (!Timed) return;
			if (Status != QuestStatus.Active) return;
			
			TimeElapsed += dt;
			if (TimeElapsed > Duration){
				Fail();
			}
		}
		
		[JsonIgnoreAttribute]
		public float Progress
		{
			get {
				int progress = 0;
				int total = 0;
				
				foreach (QuestTask task in Tasks){
					if (task.Count > 0){
						progress += task.Progress;
						total += task.Count;
					}
					
					else {
						if (task.Status == QuestTaskStatus.Completed){
							progress++;
						}
						total++;
					}
				}
				
				return (float)progress / (float)total;
			}
		}

		public JObject SerializeUserData()
		{
			JObject json = new JObject();
			json["id"] = Id;
			json["status"] = Status.ToString();
			json["time_elapsed"] = TimeElapsed.ToString();

			JArray jTasks = new JArray();
			json.Add(jTasks);
			foreach (QuestTask task in tasks)
			{
				jTasks.Add(task.SerializeUserData());
			}

			return json;
		}
		
		public void DeserializeUserData(JObject json)
		{
			if ((string)json["id"] != Id) throw new System.Exception("Cannot deserialize Quest from a different id");
			Status = (QuestStatus)System.Enum.Parse(typeof(QuestStatus), (string)json["status"]);
			TimeElapsed = float.Parse((string)json["time_elapsed"]);
		}
	}
}