using UnityEngine;
using System.Collections;
using Newtonsoft.Json;
using QuestEngine;

public class MockQuestListener : MonoBehaviour {



	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void QuestActivated(Quest quest)
	{
		Debug.Log("Quest Activated: " + quest.Name);
	}

	void QuestTasksCompleted(Quest quest)
	{
		Debug.Log("Quest Tasks Completed: " + quest.Name);
	}

	void QuestRewarding(Quest quest)
	{
		Debug.Log("Quest Rewarded: " + quest.Name);
	}

	void QuestFailed(Quest quest)
	{
		Debug.Log("Quest Failed: " + quest.Name);
	}

	void QuestUpdated(Quest quest)
	{
		Debug.Log(quest.Name + " (" + quest.Progress * 100 + "%)");
	}

	void ExecuteQuestCommand(QuestCommand command)
	{
		Debug.Log(command.Name);

		if (command.Name == "ChangeNPCScript"){
			// Change NPC's VNNode here
			Debug.Log(command.Name + ": " + command.Params["npc"] + " -> " + command.Params["node"]);
		}
	}
}
