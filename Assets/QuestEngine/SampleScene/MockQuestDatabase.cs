using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using QuestEngine;

public class MockQuestDatabase : MonoBehaviour {

	public QuestEngineController QuestEngine;
	public TextMesh Text;

	// Use this for initialization
	void Start () {
		IEnumerable<Quest> quests = QuestEngine.Database.AllQuests;
		foreach (Quest q in quests) {
			Text.text = q.Name;		
		}


		Quest quest = new Quest();
		quest.Id = "clicker";
		quest.Name = "Clicker";
		quest.Description = "Click, click, click";
		quest.Timed = false;

		QuestTask task = new QuestTask();
		task.Name = "Click the screen 5 times";
		task.Count = 5;

		QuestEvent qEvent = new QuestEvent();
		qEvent.Name = "Click";
		task.SuccessEvents.Add(qEvent);
		quest.Tasks.Add(task);

		QuestCommand command = new QuestCommand();
		command.Name = "ChangeNPCScript";
		command.Params["npc"] = "Lime";
		command.Params["node"] = "C001-A";
		quest.CompletionCommands.Add(command);

		command = new QuestCommand();
		command.Name = "UnlockDungeon";
		command.Params["id"] = "pay_dun";
		quest.ActivationCommands.Add(command);

		// Turn in
		QuestTask turnIn = new QuestTask();
		turnIn.Name = "Talk to Lime to complete the quest";
		turnIn.Count = 1;
		qEvent = new QuestEvent();
		qEvent.Name = "Click";
		turnIn.SuccessEvents.Add(qEvent);
		quest.TurnInTask = turnIn;

		QuestEngine.AttachQuest(quest);
		quest.Activate();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown(0)){
			QuestEvent qEvent = new QuestEvent();
			qEvent.Name = "Click";
			QuestEngine.HandleEvent(qEvent);
		}
	}
}
