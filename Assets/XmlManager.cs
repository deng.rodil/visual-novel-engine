using UnityEngine; 
using System.Collections; 
using System.Xml; 
using System.Xml.Serialization; 
using System.IO; 
using System.Text;
using System;

// http://www.eggheadcafe.com/articles/system.xml.xmlserialization.asp

public class XmlManager: MonoBehaviour
{
	public static object LoadInstanceAsXml (string xml, System.Type type)
	{
		if ((xml != null) && (xml.ToString () != "")) { 
			// notice how I use a reference to System.Type here, you need this 
			// so that the returned object is converted into the correct type 
			return DeserializeObject (type, xml); 
		}
		return null;
	}
	
	public static void SaveInstanceAsXml (string filename, string filePath, System.Type type, object instance)
	{
		string xml = null;
		try {
			xml = SerializeObject (type, instance);
		} catch (Exception e) {
			Debug.Log ("XmlManager: " + e);	
		}
		SaveXML (filename, xml, filePath);
	}
	
	private static string SerializeObject (System.Type type, object instance)
	{
		string xmlizedString = null; 
		MemoryStream memoryStream = new MemoryStream (); 
		XmlSerializer xs = new XmlSerializer (type);
		XmlTextWriter xmlTextWriter = new XmlTextWriter (memoryStream, new UTF8Encoding(false)); 
		xmlTextWriter.Formatting = Formatting.Indented;
		xmlTextWriter.Indentation = 4;
		
		xs.Serialize (xmlTextWriter, instance); 
		memoryStream = (MemoryStream)xmlTextWriter.BaseStream; 
		xmlizedString = UTF8ByteArrayToString (memoryStream.ToArray ()); 
		return xmlizedString; 
	}
	
	private static void SaveXML (string filename, string xml, string filePath)
	{ 
		StreamWriter writer; 
		FileInfo t = new FileInfo (filePath + filename); 
		if (!t.Exists) {
			writer = t.CreateText (); 
		} else { 
			t.Delete (); 
			writer = t.CreateText (); 
		} 
		writer.Write (xml); 
		writer.Close ();
		
		Debug.Log("XmlManager: Successfuly saved " + filePath + filename);
	}
	
	
	// Here we deserialize it back into its original form 
//	public static object DeserializeObject (System.Type type, string pXmlizedString)
//	{ 
//		XmlSerializer xs = new XmlSerializer (type); 
//		MemoryStream memoryStream = new MemoryStream (StringToUTF8ByteArray (pXmlizedString)); 
//		XmlTextWriter xmlTextWriter = new XmlTextWriter (memoryStream, Encoding.UTF8);
//		if (xmlTextWriter != null) {
//			return xs.Deserialize (memoryStream);
//		}
//		return null;
//	}
	
	public static object DeserializeObject (System.Type type, string xmlString)
	{
		XmlDocument document = new XmlDocument ();
		document.LoadXml (xmlString);
		XmlNodeReader reader = new XmlNodeReader (document);
		XmlSerializer xmlSerialzer = new XmlSerializer (type, new XmlRootAttribute (document.DocumentElement.Name));
		var result = xmlSerialzer.Deserialize (reader);
		
		return result;
	}

	// the following methods came from the referenced URL
	private static string UTF8ByteArrayToString (byte[] characters)
	{
		UTF8Encoding encoding = new UTF8Encoding (); 
		string constructedString = encoding.GetString (characters); 
		return (constructedString); 
	}
	
	private static byte[] StringToUTF8ByteArray (string pXmlString)
	{
		UTF8Encoding encoding = new UTF8Encoding (); 
		byte[] byteArray = encoding.GetBytes (pXmlString); 
		return byteArray; 
	} 	
} 
