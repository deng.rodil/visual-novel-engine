using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DialogueEngine;

public class MockListener : MonoBehaviour {

	public DialogueEngineController Engine;
	public TextMesh Dialogue;
	public TextMesh Actor;
	public TextMesh Decision;

	private Decision activeDecision;
	private Condition activeCondition;

	void Start () 
	{

	}

	void Update () 
	{
		if (Input.GetMouseButtonUp(0)){
			Engine.SendMessage("OnClientMessage", DialogueEngineController.NextDialogue);
			return;
		}

		if (activeDecision!=null){
			if (Input.GetKeyDown(KeyCode.Alpha1)) Engine.SendMessage("OnClientMessage", 0 as object);
			if (Input.GetKeyDown(KeyCode.Alpha2)) Engine.SendMessage("OnClientMessage", 1 as object);
			if (Input.GetKeyDown(KeyCode.Alpha3)) Engine.SendMessage("OnClientMessage", 2 as object);
		}
		
		if (activeCondition!=null){
			if (Input.GetKeyDown(KeyCode.C)){
				Engine.SendMessage("OnClientMessage", activeCondition.Conditions);
			} 

			// Test error data
			else if (Input.GetKeyDown(KeyCode.X)) {
				Dictionary<string,string> data = new Dictionary<string, string>();
				data["item"] = "dictionary2";
				Engine.SendMessage("OnClientMessage", data);
			}
		}

	}

	#region OnCommandStarted Events
	void OnCommandDialogue(Dialogue command)
	{
		if (!command.Append){
			Actor.text = command.Actor;
			Dialogue.text = command.Text;
		} else {
			Dialogue.text += " " + command.Text;
		}

	}

	void OnCommandDecision(Decision command)
	{
		// Store this decision
		activeDecision = command;

		// Show the choices in UI
		int count = 1;
		foreach (Response response in command.Responses){
			Decision.text += count + ") " + response.Text + "\n";
			count++;
		}

		if (command.Text != null){
			Dialogue.text = command.Text;
			Actor.text = "";
		}
	}

	void OnCommandCondition(Condition command)
	{
		activeCondition = command;
		if (command.Text != null) {
			Actor.text = "";
			Dialogue.text = command.Text;
		}
	}

	void OnCommandSpriteAction(SpriteAction command)
	{
		// Sprite code here
	}

	void OnCommandShowUI(ShowUI command)
	{
		// UI code here
	}

	void OnCommandChangeUIState(ChangeUIState command)
	{
		// UI code here
	}

	void OnCommandPlayBGM(PlayBGM command)
	{
		// audio code here
	}

	void OnCommandPlaySFX(PlaySFX command)
	{
		// audio code here
	}

	#endregion

	void OnCommandCompleted(Command command)
	{
		if (activeDecision == command){
			activeDecision = null;
			Decision.text = "";
		}

		if (activeCondition == command){
			activeCondition = null;
		}
	}

	void OnRewardReceived(Reward reward)
	{
		Debug.Log("Reward received");
	}

	void OnDeadEnd()
	{
		Actor.text = "System";
		Dialogue.text = "Dead end";
	}
}
