using System.Collections;
using System.Collections.Generic;
using System;
using System.Xml.Serialization;

namespace DialogueEngine {
	[Serializable]
	public class Response {
		public string Text { get; set; }
		public string Jump { get; set; }
		public Reward Rewards { get; set; }
	}
}