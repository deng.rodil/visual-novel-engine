using System.Collections;
using System;
using System.Xml.Serialization;

namespace DialogueEngine {
	[Serializable]
	public class PlaySFX : Command {
		
		/// <summary>
		/// Identifier for the SFX file to be played.
		/// </summary>
		/// <value>The SF.</value>
		public string SFX { get; set; }
		
		public override CommandType Type ()
		{
			return CommandType.PlaySFX;
		}
		
		public override void Execute (DialogueEngineController engine)
		{
			base.Execute (engine);
			Complete();
		}
		
		protected override string OnExecuteMessage()
		{
			return "OnCommandPlaySFX";
		}	
	}

}