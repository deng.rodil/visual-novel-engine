using System.Collections;
using System.Collections.Generic;
using System;
using System.Xml.Serialization;

namespace DialogueEngine {
	[Serializable]
	public class Condition : Command {
		[XmlIgnore]
		private bool success;
		
		[XmlElement("Conditions")]
		public SerializableDictionary<string, string> Conditions = new SerializableDictionary<string, string>();
		
		/// <summary>
		/// Text to display upon executing this condition.
		/// </summary>
		/// <value>The text.</value>
		public string Text { get; set; }
		
		/// <summary>
		/// If false, this command will be completed even if the client data does not match the parameters.
		/// </summary>
		/// <value><c>true</c> if optional; otherwise, <c>false</c>.</value>
		public bool Optional { get; set; }
		
		/// <summary>
		/// Check if this condition was fulfilled.
		/// </summary>
		/// <value><c>true</c> if success; otherwise, <c>false</c>.</value>
		public bool Success { get { return success; } }
		
		public override CommandType Type ()
		{
			return CommandType.Condition;
		}
		
		public override void HandleEvent(object obj)
		{
			Dictionary<string, string> data = obj as Dictionary<string,string>;
			
			// All parameters must match. Return immediately if a mismatch is found
			success = true;
			foreach (KeyValuePair<string,string> kv in Conditions){
				if (!data.ContainsKey(kv.Key)){
					success = false;
					break;
				}
				if (data[kv.Key] != kv.Value){
					success = false;
					break;
				}
			}
			
			// Check if the parameters matched
			if (success){
				Complete();
				return;
			}
			
			// For non-required conditions, remove reward and jump
			if (Optional){
				Reward = null;
				Jump = null;
				Complete();
			}
		}
		
		protected override string OnExecuteMessage()
		{
			return "OnCommandCondition";
		}
		
		public override System.Type HandleEventType ()
		{
			return typeof(Dictionary<string, string>);
		}
	}
}
