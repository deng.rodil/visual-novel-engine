using System.Collections;
using System;
using System.Xml.Serialization;

namespace DialogueEngine {
	[Serializable]
	public class PlayBGM : Command {
		
		/// <summary>
		/// Identifier for the BGM file to be played.
		/// </summary>
		/// <value>The background.</value>
		public string BGM { get; set; }
		
		public override CommandType Type ()
		{
			return CommandType.PlayBGM;
		}
		
		public override void Execute (DialogueEngineController engine)
		{
			base.Execute (engine);
			Complete();
		}
		
		protected override string OnExecuteMessage()
		{
			return "OnCommandPlayBGM";
		}	
	}

}