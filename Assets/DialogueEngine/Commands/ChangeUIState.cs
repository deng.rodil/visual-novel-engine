using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System;
using System.Xml.Serialization;

namespace DialogueEngine {
	[Serializable]
	public class ChangeUIState : Command {
		
		[XmlArray("UIElements")]
		public List<string> UIElements = new List<string>();
		
		/// <summary>
		/// Enables/Disables the set UIElements
		/// </summary>
		/// <value><c>true</c> if enabled; otherwise, <c>false</c>.</value>
		[XmlElement]
		public bool Enabled { get; set; }
		
		/// <summary>
		/// List of UI identifiers to enable or disable
		/// </summary>
		//public IList<string> UIElements { get { return uiElements; } }
		
		
		public override CommandType Type ()
		{
			return CommandType.ChangeUIState;
		}
		
		public override void Execute (DialogueEngineController engine)
		{
			base.Execute (engine);
			Complete();
		}
		
		protected override string OnExecuteMessage()
		{
			return "OnCommandShowUI";
		}
	}

}