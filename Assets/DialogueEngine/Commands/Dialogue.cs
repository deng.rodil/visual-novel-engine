using System.Collections;
using System;
using System.Xml.Serialization;

namespace DialogueEngine {
	public enum TextBoxOrientation
	{
		Current,
		Left,
		Center,
		Right
	}
	
	[Serializable]
	public class Dialogue  : Command{
		/// <summary>
		/// Text to be displayed.
		/// </summary>
		/// <value>The text.</value>
		public string Text { get; set; }
		/// <summary>
		/// Identifier of the actor who is responsible for this dialogue.
		/// </summary>
		/// <value>The actor.</value>
		public string Actor { get; set; }
		/// <summary>
		/// Determines whether this text will be appended to the previous text or not.
		/// </summary>
		/// <value><c>true</c> if append; otherwise, <c>false</c>.</value>
		public bool Append { get; set; }
		/// <summary>
		/// Identifier for the voiceover file to be played while this dialogue is displayed.
		/// </summary>
		/// <value>The voiceover.</value>
		public string Voiceover { get; set; }
		/// <summary>
		/// Orientation for the displayed dialogue text box
		/// </summary>
		public TextBoxOrientation Orientation;
		
		public override CommandType Type ()
		{
			return CommandType.Dialogue;
		}
		
		public override void HandleEvent(object obj)
		{
			string eventName = obj as string;
			if (eventName == "Next"){
				Complete();
			}
		}
		
		protected override string OnExecuteMessage()
		{
			return "OnCommandDialogue";
		}
		
		public override System.Type HandleEventType ()
		{
			return typeof(string);
		}
	}
}