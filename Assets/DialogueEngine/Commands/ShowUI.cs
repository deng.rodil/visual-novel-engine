using System.Collections;
using System.Collections.Generic;
using System;
using System.Xml.Serialization;

namespace DialogueEngine {
	[Serializable]
	public class ShowUI : Command {
		[XmlAttribute]
		/// <summary>
		/// Identifier of the UI to open/show.
		/// </summary>
		/// <value>The identifier.</value>
		public string Id { get; set; }
		
		[XmlElement("Parameters")]
		/// <summary>
		/// Parameters or userdata to be read by the client.
		/// </summary>
		public SerializableDictionary<string, string> Parameters = new SerializableDictionary<string, string>();
		
		public override CommandType Type ()
		{
			return CommandType.ShowUI;
		}
		
		public override void Execute (DialogueEngineController engine)
		{
			base.Execute (engine);
			Complete();
		}
		
		protected override string OnExecuteMessage()
		{
			return "OnCommandShowUI";
		}
	}
}
