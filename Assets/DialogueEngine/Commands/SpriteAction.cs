using System.Collections;
using System.Collections.Generic;
using System;
using System.Xml.Serialization;

namespace DialogueEngine {
	public enum SpriteActionMode {
		Load,
		Unload,
		ChangeFrame
	}
	
	[Serializable]
	public class SpriteAction : Command {
		/// <summary>
		/// Identifier of the actor.
		/// </summary>
		/// <value>The actor.</value>
		public string Actor { get; set; }
		/// <summary>
		/// The frame of the sprite to be used.
		/// </summary>
		/// <value>The frame.</value>
		public string Frame { get; set; }
		/// <summary>
		/// Action mode to be executed on the sprite.
		/// </summary>
		/// <value>The action.</value>
		public SpriteActionMode Action { get; set; }
		/// <summary>
		/// Determines if this command needs to wait for transition to execute the next command.
		/// </summary>
		/// <value><c>true</c> if wait for transition; otherwise, <c>false</c>.</value>
		public bool WaitForTransition { get; set; }
		
		[XmlElement("Transition")]
		/// <summary>
		/// Transition data for the sprite read by the client.
		/// </summary>
		public SerializableDictionary<string, string> Transition = new SerializableDictionary<string, string>();
		
		public override CommandType Type ()
		{
			return CommandType.SpriteAction;
		}
		
		public override void HandleEvent(object obj)
		{
			string eventName = obj as string;
			if (eventName == "TransitionFinished"){
				Complete();
			}
		}
		
		public override void Execute (DialogueEngineController engine)
		{
			base.Execute (engine);
			
			// If there is no transition or this command doesn't need to wait for TransitionFinished event, just complete the command
			if (Transition==null || Transition.Count==0 || !WaitForTransition) Complete();
		}
		
		protected override string OnExecuteMessage()
		{
			return "OnCommandSpriteAction";
		}
		
		public override System.Type HandleEventType ()
		{
			return typeof(string);
		}
	}
}