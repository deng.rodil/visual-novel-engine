using System.Collections;
using System.Collections.Generic;
using System;
using System.Xml.Serialization;

namespace DialogueEngine {
	[Serializable]
	public class Decision : Command {
		
		[XmlArray("Responses")]
		public List<Response> Responses = new List<Response>();
		
		
		//public IList<VNResponse> Responses { get { return responses; } }
		
		
		public string Text { get; set; }
		
		
		public override CommandType Type ()
		{
			return CommandType.Decision;
		}
		
		public override void HandleEvent(object obj)
		{
			int index = (int)obj;
			
			// Do not accept index that is out of bounds
			if (index >= Responses.Count || index < 0) return;
			
			// Get response and set jump and reward
			Response response = Responses[index];
			Jump = response.Jump;
			Reward = response.Rewards;
			
			Complete();
		}
		
		protected override string OnExecuteMessage()
		{
			return "OnCommandDecision";
		}
		
		public override System.Type HandleEventType ()
		{
			return typeof(int);
		}
	}
}
