﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Xml;
using System.IO;

namespace DialogueEngine {
	public class DialogueDatabase : MonoBehaviour {

        public List<TextAsset> Data;

		public Dictionary<string, Node> nodes;
		private DirectoryInfo dbDir;
		
		void Awake ()
		{
			nodes = new Dictionary<string, Node> ();
			dbDir = new DirectoryInfo(Application.dataPath + "/gamedata/dialogue");
			if (!Directory.Exists(dbDir.FullName)) Directory.CreateDirectory(dbDir.FullName);
		}

		public void LoadAll ()
		{
            // Load nodes from bundled data
            foreach (TextAsset asset in Data)
            {
                try
                {
                    Nodes nodes = (Nodes)XmlManager.DeserializeObject(typeof(Nodes), asset.text);
                    CacheNodes(nodes.nodeList);
                }
                catch (System.Exception e)
                {
                    Debug.Log(e.StackTrace);
                }
            }

            // Load nodes from gamedata folder
			foreach (FileInfo file in dbDir.GetFiles()){
				try
				{
					if (file.Extension == ".xml")
					{
						XmlDocument xmlDoc = new XmlDocument();
						xmlDoc.Load(file.FullName);	
				
						Nodes nodes = (Nodes)XmlManager.DeserializeObject(typeof(Nodes), xmlDoc.OuterXml);
					
						CacheNodes(nodes.nodeList);
					}
				}

				catch (System.Exception e)
				{
					Debug.Log(e.Message + "\n" + e.StackTrace);
				}
			}
		}

		public List<Node> GetNodes (string filename) 
		{
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.Load(Application.dataPath + "/" + filename);	
			
			Nodes nodes = (Nodes)XmlManager.DeserializeObject(typeof(Nodes), xmlDoc.OuterXml);
			
			CacheNodes(nodes.nodeList);
			
			return nodes.nodeList;
		}

		public List<Node> GetNodesFromXml (string xmlString)
		{
			Nodes nodes = (Nodes)XmlManager.DeserializeObject(typeof(Nodes), xmlString);
			
			CacheNodes(nodes.nodeList);
			
			return nodes.nodeList;
		}

		public Node GetNodeFromCache (string nodeId)
		{
			if (!nodes.ContainsKey(nodeId)) {
				Debug.Log("VNLoader.GetNodeFromCache(): Invalid Id");
				return null;
			}
			
			return nodes[nodeId];
		}

		private void CacheNodes (List<Node> nodeList)
		{
			foreach (Node node in nodeList) {
				
				if (!nodes.ContainsKey(node.Id)) {
					nodes.Add(node.Id, node);
				}
			}
		}
	}
}
