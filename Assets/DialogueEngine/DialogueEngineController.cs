using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace DialogueEngine {
	/*
 * MESSAGES
 * 	- OnDeadEnd
 * 		* Description: Reached end of node. There is no command to execute.
 * 		* Params: none
 * 	- OnCommandCompleted
 * 		* Description: A command has been completed.
 * 		* Params: VNCommand
 * 	- OnRewardReceived
 * 		* Description: A reward has been received upon completing a VNCommand
 * 		* Params: VNReward
 * 	- OnCommandDialogue
 * 		* Description: Show a new text in the dialogue box
 * 		* Params: VNDialogue
 *		* Completion: Type=string; value="Next"
 * 	- OnCommandDecision
 * 		* Description: Show a decision UI.
 * 		* Params: VNDecision
 * 		* Completion: Type=int; value=0-xx
 * 	- OnCommandCondition
 * 		* Description: Require an input from the client. Data sent to the engine must contain all the required parameters.
 * 		* Params: VNCondition
 * 		* Completion: Type=Dictionary<string,string>
 * 	- OnCommandSpriteAction
 * 		* Description: Perform an action on sprite (Load, Unload, ChangeFrame)
 * 		* Params: VNSpriteAction
 * 		* Completion: Transition (if not set, command will complete after execution)
 * 	- OnCommandShowUI
 * 		* Description: Request to show UI
 * 		* Params: VNShowUI
 * 		* Completion: None
 * 	- OnCommandChangeUIState
 * 		* Description: Enables/Disables a list of UI
 * 		* Params: VNChangeUIState
 * 		* Completion: None
 * 	- OnCommandPlayBGM
 * 		* Description: Play the bgm file 
 * 		* Params: VNPlayBGM
 * 		* Completion: None
 * 	- OnCommandPlaySFX
 * 		* Description: Play the sfx file
 * 		* Params: VNPlaySFX
 * 		* Completion: None
 * 
 **/
	
	public class DialogueEngineController : MonoBehaviour {
		
		#region ConstantMessages
		public static readonly string TransitionFinished = "TransitionFinished";
		public static readonly string NextDialogue = "Next";
		#endregion
		
		#region Inspector
		public List<GameObject> Listeners;
		#endregion

		public DialogueDatabase Database { get; private set; }
		private Node currentNode;

		
		void Awake()
		{
			// Load database here
			Database = GetComponent<DialogueDatabase>();
			Database.LoadAll();
		}
		
		/// <summary>
		/// Loads a specific node.
		/// </summary>
		/// <param name="node">Node.</param>
		public void LoadNode(Node node)
		{
			currentNode = node;
			Command command = node.NextCommand();
			if (command != null) ExecuteCommand(command);
		}
		
		public void LoadNode(string nodeId)
		{
			Node node = Database.GetNodeFromCache(nodeId);
			if (node != null) LoadNode(node);
		}
		
		void ExecuteCommand(Command command)
		{
			command.CommandCompleted += OnCommandCompleted;
			command.Execute(this);
		}
		
		public void NotifyListeners(string message)
		{
			foreach (GameObject listener in Listeners)
			{
				listener.SendMessage(message, SendMessageOptions.DontRequireReceiver);
			}
		}
		
		public void NotifyListeners(string message, object obj)
		{
			foreach (GameObject listener in Listeners)
			{
				listener.SendMessage(message, obj, SendMessageOptions.DontRequireReceiver);
			}
		}

		public void SkipCurrentCommand()
		{
			if (currentNode != null) {
				currentNode.CurrentCommand.Complete();
			}
		}
		
		#region EventHandler
		void OnCommandCompleted(Command command)
		{
			command.CommandCompleted -= OnCommandCompleted;
			NotifyListeners("OnCommandCompleted", command);
			
			// Send Rewards
			if (command.Reward != null && command.Reward.Count > 0){;
				NotifyListeners("OnRewardReceived", command.Reward);
			}
			
			// Check if we need to jump
			if (!string.IsNullOrEmpty(command.Jump)){
				currentNode = Database.GetNodeFromCache(command.Jump);
				// Return immediately if the node to jump to does not exist
				if (currentNode == null) {
					currentNode = null;;
					NotifyListeners("OnDeadEnd");
					return;
				}
			}
			
			Command nextCommand = currentNode.NextCommand();
			
			// We reached the end of node. Check if this node connects to other nodes.
			if (nextCommand == null){
				if (currentNode.Jump != null){
					currentNode = Database.GetNodeFromCache(command.Jump);
					if (currentNode != null){
						nextCommand = currentNode.NextCommand();
					}
				}
			}
			
			// Execute the command if we have one, else we declare the state as dead end
			if (nextCommand != null){
				ExecuteCommand(nextCommand);
			} else {
				currentNode = null;
				NotifyListeners("OnDeadEnd");
			}
		}
		
		void OnClientMessage(object obj)
		{
			// Don't handle if we don't have a node loaded
			if (currentNode == null) return;
			
			// Check first if the argument is compatible for the current command
			if (currentNode.CurrentCommand.HandleEventType().IsAssignableFrom(obj.GetType())){
				currentNode.CurrentCommand.HandleEvent(obj);
			}
		}
		#endregion
	}
}
