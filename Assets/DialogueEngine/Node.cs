using System.Collections;
using System.Collections.Generic;
using System;
using System.Xml.Serialization;

namespace DialogueEngine {
	[Serializable]
	public class Node {
		[XmlIgnore]
		private Command currentCommand;
		
		[XmlAttribute]
		public string Id { get; set; }
		
		[XmlArray("Commands")]
		public List<Command> commands = new List<Command>();
		
		[XmlAttribute]
		public string Jump { get; set; }
		
		[XmlIgnore]
		public Command CurrentCommand { get { return currentCommand; } }
		
		public Command NextCommand()
		{
			// If there is no command yet, get the first one
			if (currentCommand == null){
				currentCommand = commands[0];
				return currentCommand;
			}
			
			// Get next command
			int index = commands.IndexOf(currentCommand);
			if (commands.Count > index+1){
				currentCommand = commands[index+1];
				return currentCommand;
			}
			
			// If we reached the end of the list, return null
			return null;
		}
	}
	
	// Wrapper class. Used as container for serialization. Root Xml object.
	[XmlRoot]
	[Serializable]
	public class Nodes {
		
		[XmlElement("Node")]
		public List<Node> nodeList;
	}
}