using UnityEngine;
using System.Collections;
using System;
using System.Xml.Serialization;

namespace DialogueEngine {
	public enum CommandType {
		Dialogue,
		Decision,
		Condition,
		SpriteAction,
		ShowUI,
		ChangeUIState,
		PlaySFX,
		PlayBGM
	}
	
	[Serializable]
	[XmlInclude(typeof(ChangeUIState)), XmlInclude(typeof(Condition)), XmlInclude(typeof(Decision)), XmlInclude(typeof(Dialogue)), XmlInclude(typeof(PlayBGM)),
	 XmlInclude(typeof(PlaySFX)), XmlInclude(typeof(Response)), XmlInclude(typeof(ShowUI)), XmlInclude(typeof(SpriteAction))]
	public abstract class Command {
		
		[XmlIgnore]
		public System.Action<Command> CommandCompleted;
		
		public string Jump { get; set; }
		
		public Reward Reward { get; set; }
		
		public abstract CommandType Type();
		
		public void Complete()
		{
			if (CommandCompleted != null) CommandCompleted(this);
		}
		
		public virtual void HandleEvent(object obj)
		{
		}
		
		public virtual void Execute(DialogueEngineController engine)
		{
			engine.NotifyListeners(OnExecuteMessage(), this);
		}
		
		protected virtual string OnExecuteMessage()
		{
			return null;
		}
		
		public virtual System.Type HandleEventType()
		{
			return this.GetType();
		}	
	}
}